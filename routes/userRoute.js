const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");

router.post("/checkEmail", (req,res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/register", (req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/login", (req,res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

router.get("/details",auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization);

	console.log(userData);

	userController.getProfile({ userID: userData.id }).then(resultFromController => res.send(resultFromController));
});

// Route for setting an user as an Admin

router.put("/:userId/setAsAdmin", auth.verify, (req,res) => {

	console.log(req.params);
	let data = {
		userId: req.body.userId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	userController.makeAdmin(data).then(resultFromController => res.send(resultFromController));

});



// Route for creating order
router.post("/checkOut", auth.verify, (req,res) => {

	let data = {
		userID: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		productID: req.body.productID,
		quantity: req.body.quantity
	};

	userController.createOrder(data).then(resultFromController => res.send(resultFromController));
});

router.get("/:userID/userDetails", auth.verify, (req,res) => {
	userController.getUserDetails(req.params).then(resultFromController => res.send(resultFromController));

});

module.exports = router;

// route to retrieve all the orders placed by a particular user

router.get("/myOrders", auth.verify, (req,res) => {

	userController.getMyOrders(req.body.userId).then(resultFromController => res.send(resultFromController));
});

// route to retrieve all the orders (Admin only)

router.get("/allOrders", auth.verify, (req,res) => {

	let adminDetail = auth.decode(req.headers.authorization).isAdmin

	userController.getAllOrders(adminDetail).then(resultFromController => res.send(resultFromController));
});

