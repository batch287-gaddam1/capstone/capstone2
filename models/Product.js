const mongoose = require("mongoose");
const productSchema = new mongoose.Schema({
	name: {
		type: String,
		default: [ true, "Name is required."]
	},
	description: {
		type: String,
		default: [ true, "Description is required."]
	},
	price: {
		type: Number,
		default: [ true, "Price is required."]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn : {
		type: Date,
		default: new Date()
	},
	userOrders: [
		{
			userID: {
				type: String,
				required: [ true, "UserId is required."]
			},
			orderId: {
				type: String,
				required: [ true, " OrderId is required."]
			}
		}
	]
})

module.exports = mongoose.model("Product", productSchema);