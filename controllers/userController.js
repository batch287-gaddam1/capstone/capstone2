const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.checkEmailExists = (reqBody) => {
	
	return User.find({ email: reqBody.email }).then(result => {
		
		// The "find" method returns a record if a match is found
		if(result.length > 0){
			
			return true
		
		// No dublicate email found
		// The user is not yet registered in the database
		} else {
			
			return false
		}
	})
}

module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password,10)
	});

	return newUser.save().then((user,error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}

module.exports.loginUser = (reqBody) => {

	return User.findOne({ email: reqBody.email }).then(result => {

		console.log(result);

		if(result == null){
			return false;
		} else {

			const isPasswordCorrect = bcrypt.compare(reqBody.password, result.passsword);//compareSync should be here, but used just compare

			if(isPasswordCorrect){
				return { access: auth.createAccessToken(result) }
			} else{
				return false
			};
		};
	});
};

module.exports.getProfile = (data) => {

	console.log(data);//prints userID in key-value pair in the server(git bash)

	// return User.findById(data.userID).then(result => {

	// 	result.password = "";
	// 	return result;
	// });

	return User.findOne({ id: data._id }).then(result => {
		if(result == null){
			return false
		} else {
			result.password = "";
			return result;
		}
	});
};

// Controllers to make an user as an Admin

module.exports.makeAdmin = (data) => {

	console.log(data);
	if(data.isAdmin){
		let userToAdmin = {

			isAdmin: true
		}

		return User.findByIdAndUpdate( data.userId, userToAdmin).then((user,err) => {

				if(err){

					return "Error Occurred!";
				} else{

					return "You are an Admin now!"
				};	
		});
	};
	
};








module.exports.createOrder = async (data) => {

	async function getProductName(productId) {
	  try {
	    const product = await Product.findOne({ _id: productId }, 'name');
	    return product ? product.name : null; 
	  } catch (error) {
	    return null;
	  }
	};

	async function getTotalAmount(productId,productData) {
		try {
			const product = await Product.findOne({ _id: productId }, 'price');
			return product ? product.price*productData.quantity : null;
		}catch (error){
			return null;
		}
	};

	if(!data.isAdmin){

		let productData = {
			productID: data.productID,
			productName: await getProductName(data.productID),
			quantity: data.quantity
		};

		let orderData = {
			products: [productData],
			totalAmount: await getTotalAmount(data.productID,productData)
		};

		let isUserUpdated = await User.findById(data.userID).then(user => {
			user.orderedProducts.push(orderData);

			return user.save().then((user,error) => {
				if(error){
					return false;
				} else {
					return true;
				};
			});
		});

		let isProductUpdated = await Product.findById(data.productID).then(product => {
			product.userOrders.push({ userID: data.userID });

			return product.save().then((product,error) => {
				if(error){
					return false;
				} else {
					return true;
				};
			});
		});

		if(isUserUpdated && isProductUpdated){
			return "Successfully placed the Order!"
		} else {
			return "Something went wrong, Please check the details!"
		}

	} else {
		let message = Promise.resolve("Non-Admin users can access it!");
		return message.then((value) => {
			return value;
		});
	};
};

module.exports.getUserDetails = (reqParams) => {
	return User.findById(reqParams.userID).then((product,error) => {
		if(error){
			return "User doesn't exists!"
		} else {
			return product
		}
	})
};

// Controllers to retrieve all orders of a particular orders

module.exports.getMyOrders = (id) => {

		return User.find({ userId: id}).then(result => {

			return result;
		});
};

// Controllers to retrieve all orders (Admin only)

module.exports.getAllOrders = (isAdmin) => {

	if(isAdmin){

		return User.find({}).then(result => {
				return result;
		});
	};
};

