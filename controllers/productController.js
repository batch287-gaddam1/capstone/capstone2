const Product = require("../models/Product");

module.exports.addProduct = (data) => {

	console.log(data);

	if(data.isAdmin){

		let newProduct = new Product({
			name: data.product.name,
			description: data.product.description,
			price: data.product.price
		});

		return newProduct.save().then((product,error) => {

			if(error){
				return false;
			} else {
				return true;
			};
		});
	};

	let message = Promise.resolve("User must be an Admin to accept adding of product!");
	return message.then((value) => {
		return value
	});
};

module.exports.activeProducts = () => {

	return Product.find({ isActive: true }).then(result => {
		return result;
	});
};

module.exports.getAllProducts = () => {

	return Product.find({}).then (result => {
		return result;
	});
};

module.exports.getSpecificProduct = (reqParams) => {

	return Product.findById(reqParams.productID).then(result => {
		return result;
	});
};

// To update a product
module.exports.updateProduct = (reqParams,reqBody) => {

	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	return Product.findByIdAndUpdate(reqParams.productID, updatedProduct).then((product,error) => {
		if(error){
			return false;
		} else {
			return true;
		};
	});
};

// To archive a product
module.exports.archiveProduct = (reqParams) => {

	let updatedActiveField = {
		isActive: false
	};

	return Product.findByIdAndUpdate(reqParams.productID, updatedActiveField).then((product,err) => {
		if(err){
			return false;
		} else {
			return true;
		};
	});
};

// To activate a product
module.exports.activateProduct = (reqParams) => {

	let updatedActiveField = {
		isActive: true
	};

	return Product.findByIdAndUpdate(reqParams.productID, updatedActiveField).then((product,err) => {
		if(err){
			return false;
		} else {
			return true;
		};
	});
};